package characters;


import javafx.util.Pair;
import objects.GameObject;
import utils.Utils;

import java.awt.*;

public class Enemy extends BasicCharacter implements Runnable{

    private static final int PAUSE = 15;

    private final String image;
    private final int frequency;
    private final int deadOffsetY;
    private final Pair<Image, Image> deadImage;
    private int offsetX;

    private Enemy(int x, int y, int width, int height, String image, Pair<Image, Image> deadImage, int frequency, int deadOffsetY) {
        super(x, y, width, height);
        this.setToRight(true);
        this.setMoving(true);
        this.offsetX = 1;
        this.image = image;
        this.deadImage = deadImage;
        this.frequency = frequency;
        this.deadOffsetY = deadOffsetY;

        Thread chronoEnemy = new Thread(this);
        chronoEnemy.start();
    }

    public static class Builder {
        private int x;
        private int y;
        private int width;
        private int height;
        private  String image;
        private  Pair<Image, Image> deadImage;
        private int frequency;
        private int deadOffsetY;

        public Builder x(int x) {
            this.x = x;
            return this;
        }

        public Builder y(int y) {
            this.y = y;
            return this;
        }

        public Builder width(int width) {
            this.width = width;
            return this;
        }

        public Builder height(int height) {
            this.height = height;
            return this;
        }

        public Builder image(String image) {
            this.image = image;
            return this;
        }

        public Builder deadImage(Pair<String,String> deadImage) {
            this.deadImage = new Pair<>(Utils.getImage(deadImage.getKey()), Utils.getImage(deadImage.getValue()));
            return this;
        }

        public Builder frequency(int frequency) {
            this.frequency = frequency;
            return this;
        }

        public Builder deadOffsetY(int deadOffsetY) {
            this.deadOffsetY = deadOffsetY;
            return this;
        }

        public Enemy build() {
            return new Enemy(this.x, this.y, this.width, this.height, this.image, this.deadImage, this.frequency, this.deadOffsetY);
        }

    }

    public void move() {
        this.offsetX = isToRight() ? 1 : -1;
        this.setX(this.getX() + this.offsetX);

    }

    @Override
    public void run() {
        while (true) {
            if (this.alive) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void contact(GameObject obj) {
        if (this.hitAhead(obj) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(obj) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    public void contact(BasicCharacter person) {
        if (this.hitAhead(person) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(person) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    public String getImage() {
        return this.image;
    }

    public int getFrequency() {
        return this.frequency;
    }

    public int getDeadOffsetY() {
        return this.deadOffsetY;
    }

    public Image deadImage() {
        return this.isToRight() ? this.deadImage.getKey() : this.deadImage.getValue();
    }
}
