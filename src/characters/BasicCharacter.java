package characters;

import java.awt.Image;

import game.Main;
import game.Window;
import objects.GameObject;
import utils.Res;
import utils.Utils;

public abstract class BasicCharacter implements Character {

    public static final int PROXIMITY_MARGIN = 10;
    private int width, height;
    private int x, y;
    protected boolean moving;
    protected boolean toRight;
    private int counter;
    protected boolean alive;

    public BasicCharacter(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isToRight() {
        return toRight;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public Image walk(String name, int frequency) {
        String str = Res.IMG_BASE + name + (!this.moving || ++this.counter % frequency == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (this.toRight ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
        return Utils.getImage(str);
    }

    public void move() {
        if (Window.getInstance().getScene().getxPos() >= 0) {
            this.x = this.x - Window.getInstance().getScene().getMov();
        }
    }

    public boolean hitAhead(GameObject og) {
        if (this.x + this.width < og.getX() || this.x + this.width > og.getX() + 5 ||
                this.y + this.height <= og.getY() || this.y >= og.getY() + og.getHeight()) {
            return false;
        } else
            return true;
    }

    protected boolean hitBack(GameObject og) {
        if (this.x > og.getX() + og.getWidth() || this.x + this.width < og.getX() + og.getWidth() - 5 ||
                this.y + this.height <= og.getY() || this.y >= og.getY() + og.getHeight()) {
            return false;
        } else
            return true;
    }

    protected boolean hitBelow(GameObject og) {
        if (this.x + this.width < og.getX() + 5 || this.x > og.getX() + og.getWidth() - 5 ||
                this.y + this.height < og.getY() || this.y + this.height > og.getY() + 5) {
            return false;
        } else
            return true;
    }

    protected boolean hitAbove(GameObject og) {
        if (this.x + this.width < og.getX() + 5 || this.x > og.getX() + og.getWidth() - 5 ||
                this.y < og.getY() + og.getHeight() || this.y > og.getY() + og.getHeight() + 5) {
            return false;
        } else return true;
    }

    protected boolean hitAhead(BasicCharacter pers) {
        if (this.isToRight()) {
            if (this.x + this.width < pers.getX() || this.x + this.width > pers.getX() + 5 ||
                    this.y + this.height <= pers.getY() || this.y >= pers.getY() + pers.getHeight()) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    protected boolean hitBack(BasicCharacter pers) {
        if (this.x > pers.getX() + pers.getWidth() || this.x + this.width < pers.getX() + pers.getWidth() - 5 ||
                this.y + this.height <= pers.getY() || this.y >= pers.getY() + pers.getHeight())
            return false;
        return true;
    }

    public boolean hitBelow(BasicCharacter pers) {
        if (this.x + this.width < pers.getX() || this.x > pers.getX() + pers.getWidth() ||
                this.y + this.height < pers.getY() || this.y + this.height > pers.getY())
            return false;
        return true;
    }

    public boolean isNearby(BasicCharacter pers) {
        if ((this.x > pers.getX() - PROXIMITY_MARGIN && this.x < pers.getX() + pers.getWidth() + PROXIMITY_MARGIN)
                || (this.x + this.width > pers.getX() - PROXIMITY_MARGIN && this.x + this.width < pers.getX() + pers.getWidth() + PROXIMITY_MARGIN))
            return true;
        return false;
    }

    public boolean isNearby(GameObject obj) {
        if ((this.x > obj.getX() - PROXIMITY_MARGIN && this.x < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN) ||
                (this.getX() + this.width > obj.getX() - PROXIMITY_MARGIN && this.x + this.width < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN))
            return true;
        return false;
    }

    //template method
    public abstract void contact(BasicCharacter person);

    public abstract void contact(GameObject gameObject);

}
