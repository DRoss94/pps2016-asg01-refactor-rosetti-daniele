package characters;

import javafx.util.Pair;
import objects.Tunnel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.Res;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class EnemyTest {

    private static final int MUSHROOM_START_X = 800;
    private static final int MUSHROOM__START_Y = 263;
    private static final int MUSHROOM_WIDTH = 27;
    private static final int MUSHROOM_HEIGHT = 30;
    private static final int MUSHROOM_FREQUENCY = 45;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;

    private Enemy mushroom;
    @BeforeEach
    public void init() {
         mushroom = new Enemy.Builder()
                .x(MUSHROOM_START_X)
                .y(MUSHROOM__START_Y)
                .width(MUSHROOM_WIDTH)
                .height(MUSHROOM_HEIGHT)
                .image(Res.IMGP_CHARACTER_MUSHROOM)
                .deadImage(new Pair<>(Res.IMG_MUSHROOM_DEAD_DX,  Res.IMG_MUSHROOM_DEAD_SX))
                .frequency(MUSHROOM_FREQUENCY)
                .deadOffsetY(MUSHROOM_DEAD_OFFSET_Y)
                .build();
    }

    @Test
    public void moveEnemy() {
        mushroom.move();
        mushroom.move();
        assertEquals(803, mushroom.getX());
    }
}