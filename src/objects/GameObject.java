package objects;

import java.awt.Image;

import game.Main;
import game.Window;

public class GameObject {

    private int width, height;
    private int x, y;

    protected Image imgObj;

    public GameObject(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Image getImgObj() {
        return imgObj;
    }

    public void move() {
        if (Window.getInstance().getScene().getxPos() >= 0) {
            this.x = this.x - Window.getInstance().getScene().getMov();
        }
    }

}
