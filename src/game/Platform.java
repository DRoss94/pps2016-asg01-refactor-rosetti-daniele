package game;

import javafx.util.Pair;
import objects.Block;
import characters.*;

import java.awt.Graphics;
import java.awt.Image;
import java.util.*;

import javax.swing.JPanel;

import objects.GameObject;
import objects.Piece;
import objects.Tunnel;
import utils.Res;
import utils.Utils;

@SuppressWarnings("serial")
public class Platform extends JPanel {

    private static final int BACKGROUND_1_POS_X = -50;
    private static final int BACKGROUND_2_POS_X = 750;
    private static final int FLOOR_OFFSET = 293;

    private static final int MARIO_START_X = 300;
    private static final int MARIO_START_Y = 245;

    private static final int MUSHROOM_START_X = 800;
    private static final int MUSHROOM__START_Y = 263;
    private static final int MUSHROOM_WIDTH = 27;
    private static final int MUSHROOM_HEIGHT = 30;


    private static final int TURTLE_START_X = 950;
    private static final int TURTLE_START_Y = 243;
    private static final int TURTLE_WIDTH = 43;
    private static final int TURTLE_HEIGHT = 50;


    private static final int MARIO_FREQUENCY = 25;
    private static final int MUSHROOM_FREQUENCY = 45;
    private static final int TURTLE_FREQUENCY = 45;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;
    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;

    private static final int TUNNELS_NUMBER = 8;
    private static final int BLOCKS_NUMBER = 12;

    private static final int TUNNEL_STARTER_X_POSITION = 600;
    private static final int TUNNEL_STARTER_Y_POSITION = 230;
    private static final int TUNNEL_DISTANCE = 500;

    private static final int BLOCK_STARTER_X_POSITION = 400;
    private static final int BLOCK_STARTER_Y_POSITION = 180;

    private static final int PIECE_Y_DISTANCE = -30;

    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;

    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;

    private Mario mario;
    private Enemy mushroom;
    private Enemy turtle;

    private Image imgFlag;
    private Image imgCastle;

    private List<GameObject> objects;
    private List<Piece> pieces;

    public Platform() {
        super();
        this.background1PosX = BACKGROUND_1_POS_X;
        this.background2PosX = BACKGROUND_2_POS_X;
        this.mov = 0;
        this.xPos = -1;
        this.floorOffsetY = FLOOR_OFFSET;
        this.heightLimit = 0;
        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);

        mario = new Mario(MARIO_START_X, MARIO_START_Y);

        mushroom = new Enemy.Builder()
                .x(MUSHROOM_START_X)
                .y(MUSHROOM__START_Y)
                .width(MUSHROOM_WIDTH)
                .height(MUSHROOM_HEIGHT)
                .image(Res.IMGP_CHARACTER_MUSHROOM)
                .deadImage(new Pair<>(Res.IMG_MUSHROOM_DEAD_DX,  Res.IMG_MUSHROOM_DEAD_SX))
                .frequency(MUSHROOM_FREQUENCY)
                .deadOffsetY(MUSHROOM_DEAD_OFFSET_Y)
                .build();

        turtle = new Enemy.Builder()
                .x(TURTLE_START_X)
                .y(TURTLE_START_Y)
                .width(TURTLE_WIDTH)
                .height(TURTLE_HEIGHT)
                .image(Res.IMGP_CHARACTER_TURTLE)
                .deadImage(new Pair<>(Res.IMG_TURTLE_DEAD,  Res.IMG_TURTLE_DEAD))
                .frequency(TURTLE_FREQUENCY)
                .deadOffsetY(TURTLE_DEAD_OFFSET_Y)
                .build();

        objects = new ArrayList<>();
        pieces = new ArrayList<>();
        this.setTunnels();
        this.setBlocksAndPieces();

        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);

        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());
    }

    private void setTunnels() {
        for (int i=0; i<TUNNELS_NUMBER; i++) {
            this.objects.add(new Tunnel(TUNNEL_STARTER_X_POSITION + i * TUNNEL_DISTANCE, TUNNEL_STARTER_Y_POSITION));
        }
    }

    private void setBlocksAndPieces() {
        for(int i=0; i<BLOCKS_NUMBER; i++) {
            int x_shift = this.shift(800, 70);
            int y_shift = this.shift(10, -10);
            this.objects.add(new Block(BLOCK_STARTER_X_POSITION + i * x_shift, BLOCK_STARTER_Y_POSITION + y_shift));
            if(this.addPiece()) {
                this.pieces.add(new Piece(BLOCK_STARTER_X_POSITION + i * x_shift, BLOCK_STARTER_Y_POSITION + y_shift + PIECE_Y_DISTANCE));
            }
        }
    }

    private int shift(int firstValue, int secondValue) {
        double val = Math.random();
        if(val > 0.5) {
            return firstValue;
        } else {
            return secondValue;
        }
    }

    private boolean addPiece() {
        return Math.random() > 0.5;
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getxPos() {
        return xPos;
    }

    public Mario getMario() {
        return this.mario;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    public void updateBackgroundOnMovement() {
        if (this.xPos >= 0 && this.xPos <= 4600) {
            this.xPos = this.xPos + this.mov;
            // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - this.mov;
            this.background2PosX = this.background2PosX - this.mov;
        }

        if(this.background1PosX == 800 || this.background1PosX == -800) {
            this.background1PosX = -this.background1PosX;
        } else if(this.background2PosX == 800 || this.background2PosX == -800) {
            this.background2PosX = -this.background2PosX;
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = g;

        List<BasicCharacter> characters = new ArrayList<>(Arrays.asList(this.mario, this.mushroom, this.turtle));
        for(BasicCharacter basicCharacter : characters) {
            for(GameObject object : objects) {
                if(basicCharacter.isNearby(object))
                    basicCharacter.contact(object);
            }
        }

        for (int i = 0; i < pieces.size(); i++) {
            if (this.mario.contactPiece(this.pieces.get(i))) {
                Audio.playSound(Res.AUDIO_MONEY);
                this.pieces.remove(i);
            }
        }

        characters.remove(mario);
        for(int i=0; i<characters.size(); i++) {
            for(int j=0; j<characters.size(); j++) {
                if(i != j && characters.get(i).isNearby(characters.get(j))) {
                    characters.get(i).contact(characters.get(j));
                }
            }
        }

        for(BasicCharacter basicCharacter : characters) {
            if(this.mario.isNearby(basicCharacter))
                this.mario.contact(basicCharacter);
        }

        // Moving fixed objects
        this.updateBackgroundOnMovement();
        if (this.xPos >= 0 && this.xPos <= 4600) {
            for (int i = 0; i < objects.size(); i++) {
                objects.get(i).move();
            }

            for (int i = 0; i < pieces.size(); i++) {
                this.pieces.get(i).move();
            }

            this.mushroom.move();
            this.turtle.move();
        }

        g2.drawImage(this.imgBackground1, this.background1PosX, 0, null);
        g2.drawImage(this.imgBackground2, this.background2PosX, 0, null);
        g2.drawImage(this.castle, 10 - this.xPos, 95, null);
        g2.drawImage(this.start, 220 - this.xPos, 234, null);

        for (int i = 0; i < objects.size(); i++) {
            g2.drawImage(this.objects.get(i).getImgObj(), this.objects.get(i).getX(),
                    this.objects.get(i).getY(), null);
        }

        for (int i = 0; i < pieces.size(); i++) {
            g2.drawImage(this.pieces.get(i).imageOnMovement(), this.pieces.get(i).getX(),
                    this.pieces.get(i).getY(), null);
        }

        g2.drawImage(this.imgFlag, FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        g2.drawImage(this.imgCastle, CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);

        if (this.mario.isJumping())
            g2.drawImage(this.mario.doJump(), this.mario.getX(), this.mario.getY(), null);
        else
            g2.drawImage(this.mario.walk(Res.IMGP_CHARACTER_MARIO, MARIO_FREQUENCY), this.mario.getX(), this.mario.getY(), null);

        List<Enemy> enemies = new ArrayList<>(Arrays.asList(this.mushroom, this.turtle));
        for(Enemy enemy : enemies) {
            if (enemy.isAlive())
                g2.drawImage(enemy.walk(enemy.getImage(), enemy.getFrequency()), enemy.getX(), enemy.getY(), null);
            else
                g2.drawImage(enemy.deadImage(), enemy.getX(), enemy.getY() + enemy.getDeadOffsetY(), null);
        }
    }
}
